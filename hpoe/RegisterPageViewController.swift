//
//  RegisterPageViewController.swift
//  hpoe
//
//  Created by Onur YILDIRIM on 10/4/18.
//  Copyright © 2018 Onur YILDIRIM. All rights reserved.
//

import UIKit
import Firebase

class RegisterPageViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func registerButtonPressed(_ sender: UIButton) {
    
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            if error != nil {
                print(error!)
                print(error?.localizedDescription as Any)
            } else {
                print("Registration Successful.")
                self.performSegue(withIdentifier: "goToMain", sender: self)
            }
        }
    
    }
    
}
