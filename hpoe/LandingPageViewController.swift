//
//  LandingPageViewController.swift
//  hpoe
//
//  Created by Onur YILDIRIM on 10/4/18.
//  Copyright © 2018 Onur YILDIRIM. All rights reserved.
//

import UIKit

class LandingPageViewController: UIViewController {

    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        print("Login Button Preesed..")
    }
    
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        print("Register Button Pressed..")
    }
    
    
    
    
}
