//
//  LoginPageViewController.swift
//  hpoe
//
//  Created by Onur YILDIRIM on 10/4/18.
//  Copyright © 2018 Onur YILDIRIM. All rights reserved.
//

import UIKit
import Firebase

class LoginPageViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func loginButtonPressed(_ sender: UIButton) {
        Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { (authDataResult, error) in
            
            if error != nil {
                print("Error:\(error!)")
            } else {
                print("Login Successful!")
                self.performSegue(withIdentifier: "goToMain", sender: self)
            }
            
        }
    
    
    }
    
    
    
}
